import requests
from bs4 import BeautifulSoup
import psycopg2
import time

retries = 5
delay_seconds = 5
# establish connection to postgres database
for _ in range(retries):
    try:
        conn = psycopg2.connect(
            host="postgres",
            port="5432",
            dbname="netsuite",
            user="myuser",
            password="password"
        )
        break  # Connection successful, break out of loop
    except psycopg2.OperationalError:
        print("Connection failed. Retrying in {} seconds...".format(delay_seconds))
        time.sleep(delay_seconds)
else:
    print("Connection attempts exhausted. Exiting...")

# from ChatGPT
desired_objects = [
    "contact",
    "customer",
    "employee",
    "file",
    "inventoryitem",
    "invoice",
    "job",
    "noninventorysaleitem",
    "salesorder",
    "vendor"
]

for obj in desired_objects:

    print('\n\n')

    url = f'https://www.netsuite.com/help/helpcenter/en_US/srbrowser/Browser2017_2/schema/record/{obj}.html'
    page = requests.get(url).content

    soup = BeautifulSoup(page, 'html.parser')

    fields = []
    data = soup.find('div', id="contentPanel")
    

    object_name = soup.find('h1')

    field_table = data.find('table')

    objects = field_table.find_all('tr')
    for object in objects[1:]:
        columns = object.find_all('td')
        field_name = columns[0].text
        type = columns[1].text
        label = columns[3].text
        required = columns[4].text
        description = columns[5].text
        fields.append(
            {
                'field_name': field_name,
                'type': type,
                'label': label,
                'required': True if required =='T' else False,
                'description': description
            }
        )

    print(object_name.text)
    print()



    # Create table with name of object, and loop over customers to add SQL rows

    try:
        with conn:
            with conn.cursor() as db:
                print("executing create table")

                drop_table_query = f"""
                DROP TABLE IF EXISTS "{obj}"
                """
                db.execute(drop_table_query)
                create_table_query = f"""
                    CREATE TABLE IF NOT EXISTS "{obj}" (
                        name varchar(255),
                        type varchar(255),
                        label varchar(255),
                        required bool,
                        description text
                    )
                """
                db.execute(create_table_query)
                print(f"Table {obj} created successfully.")

    except Exception as e:
        print(e)
        print(f"Could not create table {obj}")

    for field in fields:

    #add each field to the current object's table    
        try:
            with conn:
                with conn.cursor() as db:
                    add_field_query = f"""
                        INSERT INTO "{obj}" (name, type, label, required, description)
                        VALUES (%s, %s, %s, %s, %s)
                    """
                    db.execute(add_field_query, (field['field_name'], field['type'], field['label'], field['required'], field['description']))
                    print(f"{field['field_name']} field added successfully to {obj} table.")
        except Exception as e:
            print(e)
            print(f"Could not add {field['field_name']} to {obj} table")

    time.sleep(1)



conn.close()