from selenium import webdriver
from pyshadow.main import Shadow
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
import psycopg2
import time
from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.service import Service as ChromeService
service = ChromeService()
# chrome_options = Options()
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('--disable-gpu')  # Disable GPU acceleration
# chrome_options.add_argument('--no-sandbox')  # Disable sandboxing for Docker



browser = webdriver.Chrome()

# retries = 5
# delay_seconds = 5
# # establish connection to postgres database
# for _ in range(retries):
#     try:
#         conn = psycopg2.connect(
#             host="postgres",
#             port="5432",
#             dbname="salesforce",
#             user="myuser",
#             password="password"
#         )
#         print("connected to postgres service")
#         break  # Connection successful, break out of loop
#     except psycopg2.OperationalError:
#         print("Connection failed. Retrying in {} seconds...".format(delay_seconds))
#         time.sleep(delay_seconds)
# else:
#     print("Connection attempts exhausted. Exiting...")



desired_objects = [
    "account",
    "asset",
    "contact",
    "contentversion",
    "contract",
    "document",
    "employee",
    "invoice",
    "lead",
    "opportunity",
    "order",
    "product2",
    "quote",
    "user"
]



shadow = Shadow(browser)

for obj in desired_objects:

    print(obj)

    browser.get(f'https://developer.salesforce.com/docs/atlas.en-us.object_reference.meta/object_reference/sforce_api_objects_{obj}.htm')
    

    shadow.set_explicit_wait(20, 3)

    element = shadow.find_element("h1")

    table = shadow.find_element('tbody')
    # <tbody>

    fields = []

    rows = shadow.get_child_elements(table)
    for row in rows:
    # <tr>
        
        
        tds = shadow.get_child_elements(row)
        field_name = tds[0]

        # print("\n\n", field_name.text)
        field_detail_boxes = shadow.get_child_elements(tds[1])
    #<dl>
        

        dts_dds = shadow.get_child_elements(field_detail_boxes[0])
        
        field = {
            "Name": "",
            "Type": "",
            "Properties": "",
            "Description": "",
        }

        prev = ""

        for x in dts_dds:
            if x.tag_name == "dt":
                prev = x.text
            elif x.tag_name == "dd":
                field[prev] = x.text
        fields.append(field)
    print(fields[0], fields[-1])
        

    # try:
    #     with conn:
    #         with conn.cursor() as db:
    #             print("executing create table")

    #             drop_table_query = f"""
    #             DROP TABLE IF EXISTS "{obj}"
    #             """
    #             db.execute(drop_table_query)
    #             create_table_query = f"""
    #                 CREATE TABLE IF NOT EXISTS "{obj}" (
    #                     name varchar(255),
    #                     type varchar(255),
    #                     properties varchar(255),
    #                     description text
    #                 )
    #             """
    #             db.execute(create_table_query)
    #             print(f"Table {obj} created successfully.")

    # except Exception as e:
    #     print(e)
    #     print(f"Could not create table {obj}")
    # for field in fields:
    #     #add each field to the current object's table    
    #     try:
    #         with conn:
    #             with conn.cursor() as db:
    #                 add_field_query = f"""
    #                     INSERT INTO "{obj}" (name, type, properties, description)
    #                     VALUES (%s, %s, %s, %s)
    #                 """
    #                 db.execute(add_field_query, (field['Name'], field['Type'], field['Properties'], field['Description']))
    #                 print(f"{field['Name']} field added successfully to {obj} table.")
    #     except Exception as e:
    #         print(e)
    #         print(f"Could not add {field['Name']} to {obj} table")

browser.quit()
# conn.close()