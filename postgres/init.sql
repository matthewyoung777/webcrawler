-- Create a user with superuser privileges
-- CREATE USER myuser WITH SUPERUSER LOGIN PASSWORD 'password';
DROP DATABASE IF EXISTS netsuite;
DROP DATABASE IF EXISTS salesforce;


-- Create the 'netsuite' database and grant privileges to the user
CREATE DATABASE netsuite;
GRANT ALL PRIVILEGES ON DATABASE netsuite TO myuser;

-- Create the 'salesforce' database and grant privileges to the user
CREATE DATABASE salesforce;
GRANT ALL PRIVILEGES ON DATABASE salesforce TO myuser;
